# Introduction
Here you can find instructions to generate the data and run one of the simulations presented in the smashGP paper. You will also have the chance to run smashGP on your own data. If you find mistakes or need assistance, don't hesitate to reach out. 

Currently, the main code is in utils/run_smashgp.R. However, some additional steps need to be followed for the code to run. The primary way to install and run this executable is through Docker, a platform designed to help developers build, share, and run modern applications. Windows and Linux based Docker instructions are provided.

The first thing to do is to download smashgp as a .zip folder and unzip it. Then, follow the instructions provided below.

## Install Docker 
Docker is a platform for containers, allowing code to be run in a virtualized environment. Thus, you need to install it first.
All commands are called from the `docker` folder.

1. First, install Docker

- If using Windows: https://docs.docker.com/desktop/install/windows-install/
- If using Linux: Depends on your distribution

2. Ensure Docker is running

- If using Windows: Run the Docker executable (open Docker)
- If using Linux: Depends on your distribution, potentially `systemctl start docker`.

3. Build the Docker Image

- If using Windows: Run the `build_windows.bat` script (You can find it in the `docker` folder)
- If using Linux: `docker` build -t smashgp .

In either case, this will create an image in which you will be able to run the smashGP code, with the required packages, requirements, etc...  If you wish to customize this image, you can change `docker/Dockerfile`. 

## Run Docker 

### Run smashGP 
- If using Windows: Run the `run_windows.bat` script (You can find it in the `docker` folder)
- If using Linux: `mkdir -p out && docker run -it --mount src="$(pwd)/out/",target="/smashgp/utils/out/",type=bind smashgp:latest /bin/sh -c "cd utils &&  Rscript run_smashgp.R"`

Doing this will generate data and run one of the simulation examples in the paper. In other words, it will generate a Perlin dataset, produce the smashGP mean and variance predictions, Dice mean and variance predections, and corresponding metrics, outputting relevant information to `docker/out`. 

We consider a 40 × 40 grid (1600 points). We use a global noise standard deviation τ=0.01, and 3 layers of Perlin noise for the dataset. We withhold 20% of the data as testing data using a “random” filter. This settings can be changed. For example, if you want to change the number of points you can use the command --size 50. This will generate data in a 50 x 50 grid. You can see the parameters that you can change in `utils/run_smashgp.R`.

Once the code runs, the terminal will display the metrics for smashGP, first, and Dice, second. Additionally, the smashGP mean predicitions for the observations in the test dataset can be found in the `docker/out` folder as `opt_pred.csv` and the variance predictions as `opt_var.csv`

### Run smashGP with your own dataset
Datasets must be provided to smashGP by placing the following files in `docker/custom_in` (examples of these files can be found in `docker/coutom_example`):
- `train_X.csv`: A m x 2 csv file where you have m training data points in R^2
- `test_X.csv`: A m x 2 csv file where you have m testing data points in R^2
- `train_y.csv`: A m x 1 csv file where you have m scalars
- `test_y.csv`: A m x 1 csv file where you have m scalars
- `init.csv`: A 2x1 csv file containing the initial lengthscale and alpha values 
- `limits.csv`: A 2x3 csv file where the first column is the initial hyperparameter guess, second is the minimum, and third is the maximum

The output files will be written out in `docker/custom_out`.

If using Windows: Run the `run_custom_windows.bat` script
If using Linux: `mkdir -p custom_out && docker run -it --mount src="$(pwd)/custom_out/",target="/smashgp/utils/out/",type=bind --mount src="$(pwd)/custom_in/",target="/smashgp/utils/in/",type=bind smashgp:latest /bin/sh -c "cd utils &&  Rscript run_smashgp_custom.R"`

### To run further commands using the smashGP image, you can access a shell through the following:

- If using Windows: run the open_windows.bat script
- If using Linux: docker run -it smashgp:latest /bin/sh


Then, once you are in the command line you can run:

```
cd utils
Rscript run_smashgp.R
```

to run the equivalent of the Perlin test described before.



# SMASH Requirements
The following packages are used by SMASH
- [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page): The linear algebra library which does much of the heavy lifting   (MPL2)
- [json](https://github.com/nlohmann/json): Used to store various information  (MIT)
- [lbfgsb_cpp_wrapper](https://github.com/constantino-garcia/lbfgsb_cpp_wrapper): Used for L-BFGS-B optimization (custom fork at libsmash_public/lbfgsb_cpp_wrapper)   (MPL2)
- [cxxopts](https://github.com/jarro2783/cxxopts): Used for CLI options for drivers  (MIT)
- [pybind11](https://pybind11.readthedocs.io/en/stable/): Used for python API [BSD Like](https://github.com/pybind/pybind11/blob/master/LICENSE)


# Binary Notes
The binary was compiled with gcc 5.3.1 (C++ 11), gfortran with openblas, targeting the sandybridge architecture.
