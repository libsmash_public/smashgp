
#Libraries required
#PACKAGES = c("tictoc","randtoolbox","corpcor","DiceKriging","psych","MaxPro","optparse","jsonlite")

#if (length(setdiff(PACKAGES, rownames(installed.packages()))) > 0) {
#  install.packages(setdiff(PACKAGES, rownames(installed.packages())))  
#}

#library(tictoc)
#library(randtoolbox)
#library(corpcor)
#library(DiceKriging)
#library(psych)
#library(MaxPro)
#library(optparse)
#library(jsonlite)

#lapply(PACKAGES, require, character.only=TRUE)
#apply(PACKAGES, library, character.only=TRUE)#

# This function takes in a prediction, ground truth, variance prediction, 
# and optionally a shift to apply to the variance, and calculates metrics

do_metrics <- function(pred, test, var=NULL, var_shift=0) {
  ret = list()
  ret$mae = mean(abs(pred - test), na.rm=TRUE)
  print("MAE")
  print(ret$mae)
  print("pred mean")
  print(ret$mae)
  print("test mean")
  print(mean(test))
  ret$rmse = sqrt(mean((pred-test)^2, na.rm=TRUE))
  if(!is.null(var)) {
    crps <- function(predlist,trueobs) {
      z <- as.numeric((trueobs - predlist$mean) / predlist$sd)
      scores <- predlist$sd * (z *(2 * pnorm(z, 0, 1) - 1) +
                                 2 * dnorm(z, 0, 1) - 1/sqrt(pi))
      return(scores)
    }
    sd = sqrt(var - var_shift)
    print("var_shift")
    print(var_shift)
    print("sd mean")
    print(mean(sd))
    ret$crps = mean(crps(list(mean=pred,sd=sd),test),na.rm=TRUE)
    
    intscore <- function(x, y, alpha=0.05) {
      hw <- -qnorm(alpha/2) * x$sd + sqrt(var_shift) 
      scores <- 2 * hw + (2/alpha) * (((x$mean - hw) - y) * (y < x$mean - hw) +
                                        (y - (x$mean + hw)) * (y > x$mean + hw))
      return(scores)
    }
    ret$int = mean(intscore(list(mean=pred,sd=sd),test),na.rm=TRUE)
    
    #CVG (larger is better)
    cvg <- function(x, y, alpha=0.05) {
      #hw <- -qnorm(alpha/2) * x$sd
      hw <- -qnorm(alpha/2) * x$sd + sqrt(var_shift) # sd=sqrt(var - var_shift) + sqrt(var_shift) != sd
      
      scores <- y >= (x$mean - hw) & y <= (x$mean + hw)
      return(scores)
    }
    ret$cvg = mean(cvg(list(mean=pred,sd=sd),test),na.rm=TRUE)
  }
  return(ret)
}
